package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();

    ResponseEntity<Object> updatePost(Long id, String stringToken, Post post);

    ResponseEntity<Object> deletePost(Long id, String stringToken);

    Iterable<Post> getUsersPosts(String stringToken);


}
